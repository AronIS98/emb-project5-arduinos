

/*

 * encoder.c

 *

 *  Created on: Jan 31, 2022

 *      Author: aroni

 */

#include <avr/io.h>

#include <avr/interrupt.h>

#include "encoder.h"

//Variables:

bool old_int0 = false;

bool old_int1 = false;

int temp_pwm_val;

float duty_glob;

int pwm_signal;

Encoder::Encoder(){

  counter = 0;

  current_enc2 = false;

  current_enc1 = false;

  old_enc1 = false;

  old_enc2 = false;

  enc1_change = false;

  enc2_change = false;

    

}

void Encoder::init(){

    DDRD &= ~(1 << DDD2); // set the PD2 pin as input

    DDRD &= ~(1 << DDD3); // set the PD3 pin as input

    PORTD |= (1 << PORTD2); // enable pull-up resistor on PD2

    PORTD |= (1 << PORTD3); // enable pull-up resistor on PD3

    EICRA |= (1 << ISC00) | (1 << ISC01); // set INT0 to trigger on RISING edge.
    EICRA |= (1 << ISC10) | (1 << ISC11); // set INT0 to trigger on RISING edge.

    EIMSK |= (1 << INT0);
    EIMSK |= (1 << INT1);

    sei(); // turn on interrupts

}



